package com.homework.bookmanagement.repository;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.dto.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesRepository {
    @Insert("INSERT INTO tb_categories(title) VALUES(#{title})")
    boolean insert(Category category);

    @Select("SELECT * FROM tb_categories ORDER BY id ASC LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    List<Category> findAll(@Param("pagination")Pagination pagination);

    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    boolean delete(int id);

    @Select("SELECT * FROM tb_categories WHERE id=#{id}")
    Category findByID(int id);

    @Update("UPDATE tb_categories SET title=#{category.title} WHERE id=#{id}")
    boolean update(int id,Category category);

    @Select("SELECT id FROM tb_categories WHERE id=#{id}")
    Category findID(int id);

    @Select("SELECT COUNT(id) FROM tb_categories")
    int countAllCategory();
}
