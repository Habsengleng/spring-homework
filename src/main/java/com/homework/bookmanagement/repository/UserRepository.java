package com.homework.bookmanagement.repository;

import com.homework.bookmanagement.repository.dto.RoleDto;
import com.homework.bookmanagement.repository.dto.UserDto;
import org.apache.ibatis.annotations.*;
import org.postgresql.util.PSQLException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

//    @InsertProvider(type = UserProvider.class, method = "insertUserSql")
    @Insert("INSERT INO tb_user(user_id,username,password) VALUES(#{userid},#{username},#{password})")
    boolean insert(UserDto userDto) throws PSQLException;
//
//    @InsertProvider(type = UserProvider.class, method = "createUserRolesSql")
    @Insert("INSERT INTO tbuser_role(user_id,role_id) VALUES(user.id,role.id)")
    boolean createUserRoles(UserDto user, RoleDto role);

//    @SelectProvider(type = UserProvider.class, method = "selectIdByUserId")
    @Select("SELECT id FROM tb_user WHERE users_id=#{userId}")
    int selectIdByUserId(String userId);
//
//    @SelectProvider(type = UserProvider.class, method = "selectUserByUsernameSql")
//    @Results({
//            @Result(column = "user_id", property = "userId"),
//            @Result(column = "id",
//                    property = "roles",
//            many = @Many(select = "selectRolesByUserId"))
//    })
//    UserDto selectUserByUsername(String username);
//
//    @Select("select r.id, r.name from roles r\n" +
//            "inner join users_roles ur on r.id = ur.role_id\n" +
//            "where ur.user_id = #{id}")
//    List<RoleDto> selectRolesByUserId(int id);

}
