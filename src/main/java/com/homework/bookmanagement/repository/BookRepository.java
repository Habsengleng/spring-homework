package com.homework.bookmanagement.repository;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.dto.BookDto;
import com.homework.bookmanagement.repository.dto.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    //Repository
    @Select("SELECT * FROM tb_books ORDER BY id ASC LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    @Results({
            @Result(column = "category_id", property ="category",many = @Many(select = "selectCateByID"))
    })
    List<BookDto> findAll(@Param("pagination") Pagination pagination);
    @Select("SELECT * FROM tb_categories WHERE id=#{category_id}")
    Category selectCateByID(int category_id);

    @Select("SELECT COUNT(id) FROM tb_books")
    int countAllBooks();

//    @InsertProvider(value = BookProvider.class, method = "insertBookSql")
    @Insert("INSERT INTO tb_books(title,author,description,thumbnail,category_id) VALUES(#{title},#{author},#{description},#{thumbnail},#{category.id})")
    @Results({
            @Result(column = "category_id",property = "category")
    })
    boolean insert(BookDto bookDto);

    @Update("UPDATE tb_books SET title=#{bookDto.title}, author=#{bookDto.author}, description=#{bookDto.description}, thumbnail=#{bookDto.thumbnail},category_id=#{bookDto.category.id} WHERE id=#{id}")
//    @Results({
//            @Result(column = "category_id",property = "category")
//    })
    boolean update(int id,BookDto bookDto);

    @Delete("DELETE FROM tb_books WHERE id=#{id}")
    boolean delete(int id);

    @Select("SELECT * FROM tb_books WHERE id=#{id}")
    @Results({
            @Result(column = "category_id", property ="category",many = @Many(select = "selectCateByID"))
    })
    BookDto findByID(int id);

    @Select("SELECT * FROM tb_books WHERE title LIKE '%'||#{filter.title}||'%'")
    @Results({
            @Result(column = "category_id", property ="category",many = @Many(select = "selectCateByID"))
    })
    BookDto selectByTitle(String title);

    @Select("SELECT * FROM tb_books WHERE category_id=#{id} LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
    @Results({
            @Result(column = "category_id", property ="category",many = @Many(select = "selectCateByID"))
    })
    List<BookDto> selectByCateId(int id,Pagination pagination);
}
