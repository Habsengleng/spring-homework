package com.homework.bookmanagement.rest.restController;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class FileUploadRestController {

    @PostMapping("/upload")
//    public String upLoadFile(@RequestParam("file")MultipartFile file){
//        File uploadedFile = new File("D:\\HRD-Center\\HRD-JAVA\\Spring FIle\\book-management\\src\\main\\resources\\Image\\"+file.getOriginalFilename());
//
//        try {
//            uploadedFile.createNewFile();
//            FileOutputStream fileOutputStream = new FileOutputStream(uploadedFile);
//            fileOutputStream.write(file.getBytes());
//            fileOutputStream.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return "localhost:8080/image/"+uploadedFile.getName();
//    }
    public Map<String, Object> upLoadFile(@RequestParam("file")MultipartFile file){
        Map<String, Object> result = new HashMap<>();
        File uploadedFile = new File("D:\\HRD-Center\\HRD-JAVA\\Spring FIle\\book-management\\src\\main\\resources\\Image\\"+file.getOriginalFilename());
        try {
            uploadedFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(uploadedFile);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        result.put("data","localhost:8080/image/"+uploadedFile.getName());
        result.put("message","Upload file successfully");
        result.put("status",true);
        return result;
    }

}
