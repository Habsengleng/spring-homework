package com.homework.bookmanagement.rest.restController;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.dto.BookDto;
import com.homework.bookmanagement.repository.dto.Category;
import com.homework.bookmanagement.rest.request.BookRequest;
import com.homework.bookmanagement.rest.response.BaseApiResponse;
import com.homework.bookmanagement.rest.response.BookResponse;
import com.homework.bookmanagement.service.BookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BookRestController {
    private BookService bookService;
    @Autowired
    public void setBookService(BookService bookService){
        this.bookService=bookService;
    }
    //Book Controller
    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<BookResponse>>> select(@RequestParam(value = "page", required = true,defaultValue = "1")int page,
                                                    @RequestParam(value = "limit",required = true,defaultValue = "4")int limit){
        ModelMapper mapper = new ModelMapper();
        Pagination pagination = new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();

        pagination.setTotalCount(bookService.countAllBooks());
        pagination.setTotalPages(pagination.getTotalPages());
        BaseApiResponse<List<BookResponse>> response = new BaseApiResponse<>();
            List<BookDto> bookDtos = bookService.findAll(pagination);

            List<BookResponse>bookResponses =new ArrayList<>();
            for(BookDto bookDto:bookDtos){
                bookResponses.add(mapper.map(bookDto,BookResponse.class));
            }

            response.setMessage("You have retrieved all the d" +
                    "ata");
            response.setPagination(pagination);
            response.setData(bookResponses);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            response.setStatus(HttpStatus.OK);
            return ResponseEntity.ok(response);
        }
        @RequestMapping(value = "/books/{id}",method = RequestMethod.GET)
        public Map<String, Object> getBookById(@PathVariable("id") int id){
            Map<String, Object> result = new HashMap<>();
            BookDto bookDto = bookService.findById(id);
            try{
                if (bookDto==null){
                    result.put("Message", "Title of Book Not Found");
                    result.put("Response Code", "404");
                }else{
                    result.put("Data", bookDto);
                    result.put("Message", "Search Title of Book Successfully");
                    result.put("Response Code", "200");
                }
            }catch (Exception e){
                e.getMessage();
            }
            return result;
        }
    @RequestMapping(value = "/books", method = RequestMethod.GET,params = "title")
    public Map<String, Object> searchBookByTitle(@RequestParam String title){
        Map<String, Object> result = new HashMap<>();
        BookDto bookDto = bookService.selectByTitle(title);
        try{
            if (bookDto==null){
                result.put("Message", "Title of Book Not Found");
                result.put("Response Code", "404");
            }else{
                bookService.selectByTitle(title);
                result.put("Data", bookDto);
                result.put("Message", "Search Title of Book Successfully");
                result.put("Response Code", "200");
            }
        }catch (Exception e){
            e.getMessage();
        }
        return result;
    }
    @RequestMapping(value = "/books",method = RequestMethod.GET,params = "categoryID")
    public ResponseEntity<BaseApiResponse<List<BookResponse>>> selectByCateID(@RequestParam(value = "page", required = false,defaultValue = "1")int page,
                                                                              @RequestParam(value = "limit",required = false,defaultValue = "4")int limit,
                                                                              @RequestParam("categoryID") int cateID){
        ModelMapper mapper = new ModelMapper();
        Pagination pagination= new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();

        pagination.setTotalCount(bookService.countAllBooks());
        pagination.setTotalPages(pagination.getTotalPages());
        BaseApiResponse<List<BookResponse>> response = new BaseApiResponse<>();
        List<BookDto> bookDtos = bookService.selectByCateId(cateID,pagination);

        List<BookResponse>bookResponses =new ArrayList<>();
        for(BookDto bookDto:bookDtos){
            bookResponses.add(mapper.map(bookDto,BookResponse.class));
        }
        response.setMessage("Retrieved data by category succesfully");
        response.setPagination(pagination);
        response.setData(bookResponses);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);

    }

    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookRequest>> insert(@RequestBody BookRequest bookRequest){
        BaseApiResponse<BookRequest> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = mapper.map(bookRequest,BookDto.class);

        BookDto result = bookService.insert(bookDto);
        BookRequest results = mapper.map(result,BookRequest.class);
        response.setMessage("You have posted category successfully");
        response.setData(results);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @PutMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookRequest>> update(@PathVariable("id") int id, @RequestBody BookRequest bookRequest){
        BaseApiResponse<BookRequest> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        BookDto bookDto =mapper.map(bookRequest,BookDto.class);
        BookDto results=bookService.update(id,bookDto);
        System.out.println(results);
        BookRequest bookRequests = mapper.map(results,BookRequest.class);
        response.setMessage("You have updated category successfully");
        response.setData(bookRequests);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookDto>> delete(@PathVariable("id") int id){
        BaseApiResponse<BookDto> response = new BaseApiResponse<>();
        BookDto bookDto = bookService.findById(id);
        bookService.delete(id);
        response.setMessage("Category was deleted succesfully");
        response.setData(bookDto);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
}
