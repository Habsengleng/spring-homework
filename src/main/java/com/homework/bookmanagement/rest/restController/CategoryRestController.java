package com.homework.bookmanagement.rest.restController;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.dto.Category;
import com.homework.bookmanagement.rest.request.CategoryRequest;
import com.homework.bookmanagement.rest.response.BaseApiResponse;
import com.homework.bookmanagement.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CategoryRestController {
    private CategoryService categoryService;
    @Autowired
    public void setCategoryService(CategoryService categoryService){
        this.categoryService=categoryService;
    }

    @PostMapping("/categories")
    public ResponseEntity<BaseApiResponse<CategoryRequest>> insert(@RequestBody CategoryRequest categoryRequest){
        BaseApiResponse<CategoryRequest> response = new BaseApiResponse<>();
            ModelMapper mapper = new ModelMapper();
            Category category =mapper.map(categoryRequest,Category.class);
            Category results = categoryService.insert(category);
            CategoryRequest categories = mapper.map(results,CategoryRequest.class);

            response.setMessage("You have posted category successfully");
            response.setData(categories);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories")
    public ResponseEntity<BaseApiResponse<List<Category>>> select(@RequestParam(value = "page", required = false,defaultValue = "1")int page,
                                                                  @RequestParam(value = "limit",required = false,defaultValue = "4")int limit){

        Pagination pagination = new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();

        pagination.setTotalCount(categoryService.countAllCategory());
        pagination.setTotalPages(pagination.getTotalPages());

        BaseApiResponse<List<Category>> response = new BaseApiResponse<>();
        List<Category>  categories = categoryService.findAll(pagination);

        response.setMessage("You have retrieved all the d" +
                "ata");
        response.setPagination(pagination);
        response.setData(categories);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<Category>> delete(@PathVariable("id") int id){
        BaseApiResponse<Category> response = new BaseApiResponse<>();
        Category category =categoryService.findById(id);
        categoryService.delete(id);
        response.setMessage("Category was deleted succesfully");
        response.setData(category);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
    return ResponseEntity.ok(response);
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequest>> update(@PathVariable("id") int id, @RequestBody CategoryRequest categoryRequest){
        BaseApiResponse<CategoryRequest> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        Category category =mapper.map(categoryRequest,Category.class);
        Category category1=categoryService.findID(id);
        if(category1==null){
            response.setMessage("ID not found");
        }else{
            Category results=categoryService.update(id,category);
            CategoryRequest categories = mapper.map(results,CategoryRequest.class);
            response.setMessage("You have updated category successfully");
            response.setData(categories);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            response.setStatus(HttpStatus.OK);
        }
        return ResponseEntity.ok(response);
    }
}
