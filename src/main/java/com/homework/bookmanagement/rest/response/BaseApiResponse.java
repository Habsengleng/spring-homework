package com.homework.bookmanagement.rest.response;

import com.homework.bookmanagement.Utilities.Pagination;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class BaseApiResponse<T> {
    private String message;
    private Pagination pagination;
    private T data;
    private HttpStatus status;
    private Timestamp time;

    public BaseApiResponse() {}

    public BaseApiResponse(String message, Pagination pagination, T data, HttpStatus status, Timestamp time) {
        this.message = message;
        this.pagination = pagination;
        this.data = data;
        this.status = status;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "BaseApiResponse{" +
                "message='" + message + '\'' +
                ", pagination=" + pagination +
                ", data=" + data +
                ", status=" + status +
                ", time=" + time +
                '}';
    }
}
