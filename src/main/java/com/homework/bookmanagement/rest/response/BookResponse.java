package com.homework.bookmanagement.rest.response;

import com.homework.bookmanagement.repository.dto.BookDto;
import com.homework.bookmanagement.repository.dto.Category;
import com.homework.bookmanagement.rest.request.CategoryRequest;

public class BookResponse {
    private int id;
    private String title;
    private String author;
    private String description;
    private String thumbnail;
    private Category category;

    public BookResponse(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public BookResponse(int id, String title, String author, String description, String thumbnail, Category category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
        this.category = category;
    }
}
