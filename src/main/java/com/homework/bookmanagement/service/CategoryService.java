package com.homework.bookmanagement.service;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.dto.Category;

import java.util.List;

public interface CategoryService {

    Category insert(Category category);
    List<Category> findAll(Pagination pagination);
    boolean delete(int id);
    Category findById(int id);
    Category update(int id,Category category);
    Category findID(int id);
    int countAllCategory();

}
