package com.homework.bookmanagement.service;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.dto.BookDto;

import java.awt.print.Book;
import java.util.List;

public interface BookService {
    List<BookDto> findAll(Pagination pagination);
    int countAllBooks();
    BookDto insert(BookDto bookDto);
    BookDto update(int id, BookDto bookDto);
    boolean delete(int id);
    BookDto findById(int id);
    BookDto selectByTitle(String title);
    List<BookDto> selectByCateId(int id,Pagination pagination);
}
