package com.homework.bookmanagement.service.Imple;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.BookRepository;
import com.homework.bookmanagement.repository.dto.BookDto;
import com.homework.bookmanagement.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.print.Book;
import java.util.List;

@Service
public class BookServiceImple implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository){
        this.bookRepository=bookRepository;
    }
    //Serivice implement
    @Override
    public List<BookDto> findAll(Pagination pagination) {
        return bookRepository.findAll(pagination);
    }

    @Override
    public int countAllBooks() {
        return bookRepository.countAllBooks();
    }

    @Override
    public BookDto insert(BookDto bookDto) {
        boolean isInserted = bookRepository.insert(bookDto);
        if(isInserted) return bookDto;
        else return null;
    }

    @Override
    public BookDto update(int id, BookDto bookDto) {
        boolean isUpdate = bookRepository.update(id,bookDto);
        if(isUpdate) return bookDto;
        else return null;
    }

    @Override
    public boolean delete(int id) {
        return bookRepository.delete(id);
    }

    @Override
    public BookDto findById(int id) {
        return bookRepository.findByID(id);
    }

    @Override
    public BookDto selectByTitle(String title) {
        return bookRepository.selectByTitle(title);
    }

    @Override
    public List<BookDto> selectByCateId(int id,Pagination pagination) {
        return bookRepository.selectByCateId(id,pagination);
    }
}
