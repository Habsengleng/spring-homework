package com.homework.bookmanagement.service.Imple;

import com.homework.bookmanagement.Utilities.Pagination;
import com.homework.bookmanagement.repository.CategoriesRepository;
import com.homework.bookmanagement.repository.dto.Category;
import com.homework.bookmanagement.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImple implements CategoryService {
    private CategoriesRepository categoriesRepository;
    @Autowired
    public void setCategoriesRepository(CategoriesRepository categoriesRepository){
        this.categoriesRepository=categoriesRepository;
    }

    @Override
    public Category insert(Category category) {
        boolean isInserted = categoriesRepository.insert(category);
        if(isInserted) return category;
        else return null;
    }

    @Override
    public List<Category> findAll(Pagination pagination) {
        return categoriesRepository.findAll(pagination);
    }

    @Override
    public boolean delete(int id) {
        return categoriesRepository.delete(id);
    }

    @Override
    public Category findById(int id) {
        return categoriesRepository.findByID(id);
    }

    @Override
    public Category update(int id,Category category) {
        boolean isUpdate = categoriesRepository.update(id,category);
        if(isUpdate) return category;
        else return null;
    }

    @Override
    public Category findID(int id) {
        return categoriesRepository.findID(id);
    }

    @Override
    public int countAllCategory() {
        return categoriesRepository.countAllCategory();
    }
}
